<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PostController extends Controller
{
    public function index()
    {
        $baseUrl = env('COS_BASE_URI_URL');
        $data = Http::get($baseUrl)->json();
        $collection = collect($data);
        $uniquePosts = $collection->unique('userId');
        //dump($data);
        if(!$data)
            return back()->with('error', 'no data received');
        return view('index', [
            'uniquePosts' => $uniquePosts
        ]);
    }
    public function show($id)
    {
        $baseUrl = env('COS_BASE_URI_URL');
        $data = Http::get($baseUrl)->json();
        $collection = collect($data);
        $uniquePostsBody = $collection->unique('userId');
        //dump($uniquePostsBody);
        if(!$data)
            return back()->with('error', 'no data received');
        return view('show', [
            'uniquePostsBody' => $uniquePostsBody,
            'id' => $id
        ]);
    }
}
