@extends('app')
@section('content')
  @foreach ($uniquePosts as $uniquePost)
    <div class="post-preview">
      <a href="{{ route('show', $uniquePost['userId']) }}">
          <h2 class="post-title">{{ $uniquePost['userId'] }}</h2>
          <h3 class="post-subtitle">{{ Str::limit($uniquePost['title'], 50) }}</h3>
      </a>
      <p class="post-meta">
          Posted by
          <a href="#!">Start Bootstrap</a>
          on September 24, 2021
      </p>
    </div>
    <hr class="my-4" />
  @endforeach
@endsection