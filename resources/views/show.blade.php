@extends('app')
@section('content') 
    @foreach ($uniquePostsBody as $postBody)
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-12 col-xl-12">
                    <p>
                       {{ $postBody['body'] }}
                    </p>
            </div>
        </div>
    </div>
    @endforeach
@endsection